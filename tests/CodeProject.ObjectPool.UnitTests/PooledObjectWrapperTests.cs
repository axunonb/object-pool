﻿// Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System.Collections.Generic;
using NUnit.Framework;
using Shouldly;

namespace CodeProject.ObjectPool.UnitTests
{
    [TestFixture, Parallelizable]
    internal sealed class PooledObjectWrapperTests
    {
        [Test]
        public void Constructor_InternalResourceIsNotNull_ShouldBeAssignedToProperty()
        {
            // Arrange
            var internalResource = new List<int>();

            // Act
            var wrapper = new PooledObjectWrapper<List<int>>(internalResource);

            // Assert
            wrapper.InternalResource.ShouldBeSameAs(internalResource);
        }

        [Test]
        public void Create_InternalResourceIsNotNull_ShouldBeAssignedToProperty()
        {
            // Arrange
            var internalResource = new List<int>();

            // Act
            var wrapper = PooledObjectWrapper.Create(internalResource);

            // Assert
            wrapper.InternalResource.ShouldBeSameAs(internalResource);
        }
    }
}
