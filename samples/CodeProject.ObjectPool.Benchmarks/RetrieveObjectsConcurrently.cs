﻿// Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System;
using System.Threading.Tasks;
using BenchmarkDotNet.Attributes;
using CodeProject.ObjectPool.MicrosoftExtensionsAdapter;

namespace CodeProject.ObjectPool.Benchmarks
{
    [Config(typeof(Program.Config))]
    public class RetrieveObjectsConcurrently
    {
        private readonly Microsoft.Extensions.ObjectPool.ObjectPool<MyResource> _adaptedMicrosoftObjectPool =
            ObjectPoolAdapter.CreateForPooledObject(
                new ObjectPool<MyResource>(21, () => new MyResource(DateTime.UtcNow.ToString()))
            );
        private readonly Microsoft.Extensions.ObjectPool.ObjectPool<MyResource> _microsoftObjectPool =
            new Microsoft.Extensions.ObjectPool.DefaultObjectPoolProvider().Create(
                new MyResource.Policy()
            );
        private readonly ObjectPool<MyResource> _objectPool =
            new(21, () => new MyResource(DateTime.UtcNow.ToString()));
        private readonly ParameterizedObjectPool<int, MyResource> _paramObjectPool =
            new(21, x => new MyResource(DateTime.UtcNow + "#" + x));

        [Params(10, 100, 1000)]
        public int Count { get; set; }

        [Benchmark]
        public ParallelLoopResult AdaptedMicrosoft() =>
            Parallel.For(
                0,
                Count,
                _ =>
                {
                    MyResource? res = null;
                    string str;
                    try
                    {
                        res = _adaptedMicrosoftObjectPool.Get();
                        str = res.Value;
                    }
                    finally
                    {
                        if (res != null)
                        {
                            _adaptedMicrosoftObjectPool.Return(res);
                        }
                    }
                }
            );

        [Benchmark]
        public ParallelLoopResult Microsoft() =>
            Parallel.For(
                0,
                Count,
                _ =>
                {
                    MyResource? res = null;
                    string str;
                    try
                    {
                        res = _microsoftObjectPool.Get();
                        str = res.Value;
                    }
                    finally
                    {
                        if (res != null)
                        {
                            _microsoftObjectPool.Return(res);
                        }
                    }
                }
            );

        [Benchmark]
        public ParallelLoopResult Parameterized() =>
            Parallel.For(
                0,
                Count,
                _ =>
                {
                    string str;
                    using var x = _paramObjectPool.GetObject(21);
                    str = x.Value;
                }
            );

        [Benchmark(Baseline = true)]
        public ParallelLoopResult Simple() =>
            Parallel.For(
                0,
                Count,
                _ =>
                {
                    string str;
                    using var x = _objectPool.GetObject();
                    str = x.Value;
                }
            );

        private sealed class MyResource : PooledObject
        {
            public MyResource(string value)
            {
                Value = value;
            }

            public string Value { get; }

            public sealed class Policy
                : Microsoft.Extensions.ObjectPool.IPooledObjectPolicy<MyResource>
            {
                public MyResource Create() => new(DateTime.UtcNow.ToString());

                public bool Return(MyResource obj) => true;
            }
        }
    }
}
