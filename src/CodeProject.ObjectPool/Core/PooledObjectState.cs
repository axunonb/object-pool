﻿// Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

namespace CodeProject.ObjectPool.Core
{
    /// <summary>
    ///   Describes the state of a pooled object.
    /// </summary>
    public enum PooledObjectState
    {
        /// <summary>
        ///   The object is inside the pool, waiting to be used.
        /// </summary>
        Available = 0,

        /// <summary>
        ///   The object is outside the pool, waiting to return to the pool.
        /// </summary>
        Unavailable = 1,

        /// <summary>
        ///   The object has been disposed and cannot be used anymore.
        /// </summary>
        Disposed = 2
    }
}
