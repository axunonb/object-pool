﻿// Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System;
using System.Text;

namespace CodeProject.ObjectPool.Specialized
{
    /// <summary>
    ///   An <see cref="IObjectPool{PooledStringBuilder}"/> ready to be used.
    ///   <see cref="StringBuilder"/> management can be further configured using the
    ///   <see cref="MinimumStringBuilderCapacity"/> and <see cref="MaximumStringBuilderCapacity"/> properties.
    /// </summary>
    public sealed class StringBuilderPool : ObjectPool<PooledStringBuilder>, IStringBuilderPool
    {
        /// <summary>
        ///   Default maximum string builder capacity. Shared by all
        ///   <see cref="IStringBuilderPool"/> instances, defaults to 524288 characters.
        /// </summary>
        public const int DefaultMaximumStringBuilderCapacity = 512 * 1024;

        /// <summary>
        ///   Default minimum string builder capacity. Shared by all
        ///   <see cref="IStringBuilderPool"/> instances, defaults to 4096 characters.
        /// </summary>
        public const int DefaultMinimumStringBuilderCapacity = 4 * 1024;

        /// <summary>
        ///   Backing field for <see cref="MaximumStringBuilderCapacity"/>.
        /// </summary>
        private int _maximumItemCapacity = DefaultMaximumStringBuilderCapacity;

        /// <summary>
        ///   Backing field for <see cref="MinimumStringBuilderCapacity"/>.
        /// </summary>
        private int _minimumItemCapacity = DefaultMinimumStringBuilderCapacity;

        /// <summary>
        ///   Builds the specialized pool.
        /// </summary>
        public StringBuilderPool()
            : base(ObjectPool.DefaultPoolMaximumSize, (Func<PooledStringBuilder>?)null)
        {
            FactoryMethod = () => new PooledStringBuilder(MinimumStringBuilderCapacity);
        }

        /// <summary>
        ///   Thread-safe pool instance.
        /// </summary>
        public static IStringBuilderPool Instance { get; } = new StringBuilderPool();

        /// <summary>
        ///   Maximum capacity a <see cref="StringBuilder"/> might have in order to be able to
        ///   return to pool. Defaults to <see cref="DefaultMaximumStringBuilderCapacity"/>.
        /// </summary>
        public int MaximumStringBuilderCapacity
        {
            get { return _maximumItemCapacity; }
            set
            {
                var oldValue = _maximumItemCapacity;
                _maximumItemCapacity = value;
                if (oldValue > value)
                {
                    Clear();
                }
            }
        }

        /// <summary>
        ///   Minimum capacity a <see cref="StringBuilder"/> should have when created and this is
        ///   the minimum capacity of all builders stored in the pool. Defaults to <see cref="DefaultMinimumStringBuilderCapacity"/>.
        /// </summary>
        public int MinimumStringBuilderCapacity
        {
            get { return _minimumItemCapacity; }
            set
            {
                var oldValue = _minimumItemCapacity;
                _minimumItemCapacity = value;
                if (oldValue < value)
                {
                    Clear();
                }
            }
        }

        /// <summary>
        ///   Returns a pooled string builder using given string as initial value.
        /// </summary>
        /// <param name="value">The string used to initialize the value of the instance.</param>
        /// <returns>A pooled string builder.</returns>
        public PooledStringBuilder GetObject(string value)
        {
            var psb = GetObject();
            psb.StringBuilder.Append(value);
            return psb;
        }
    }
}
